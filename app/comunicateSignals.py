from PySide2.QtCore import QObject, Signal


class CommunicateSignals(QObject):
    buttonAddScriptSignal = Signal()


CommunicateSignals = CommunicateSignals()
