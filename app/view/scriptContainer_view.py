from PySide2.QtCore import Signal
from app.ui.ui_script_container import Ui_ScriptContainer
from PySide2.QtWidgets import QWidget


class ScriptContainerWidgetView(QWidget, Ui_ScriptContainer):
    buttonRunScriptSignal = Signal(str)

    def __init__(self):
        super().__init__()
        self.setupUi(self)
        self.buttonRunScript.clicked.connect(self.btn_click)

    def btn_click(self):
        self.buttonRunScriptSignal.emit(self.plainTextScript.toPlainText())