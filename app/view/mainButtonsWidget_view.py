from app.ui.ui_main_buttons import Ui_MainButtons
from PySide2.QtWidgets import QWidget
from app.comunicateSignals import CommunicateSignals


class MainButtonsWidgetView(QWidget, Ui_MainButtons):
    def __init__(self):
        super().__init__()
        self.setupUi(self)
        self.buttonAddScript.clicked.connect(self.btn_click)

    @staticmethod
    def btn_click():
        CommunicateSignals.buttonAddScriptSignal.emit()
