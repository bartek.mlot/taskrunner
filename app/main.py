import sys
from PySide2.QtWidgets import QApplication, QMainWindow, QWidget, QVBoxLayout
from app.layoutScriptContainerWidget import LayoutScriptContainerWidget
from app.ui.ui_main import Ui_TaskRunner
from app.widget.mainButtonsWidget import MainButtonsWidget


class MainWindow(QMainWindow, Ui_TaskRunner):
    def __init__(self):
        super().__init__()
        self.setupUi(self)
        main_widget = QWidget()
        self.setCentralWidget(main_widget)
        self.layout = QVBoxLayout()
        self.addWidgetsToLayout()
        main_widget.setLayout(self.layout)

    def addWidgetsToLayout(self):
        main_buttons_widget = MainButtonsWidget()
        self.layout.addWidget(main_buttons_widget.view)

        LayoutScriptContainerWidget(self.layout)


def main():
    app = QApplication(sys.argv)
    window = MainWindow()
    window.show()
    sys.exit(app.exec_())


if __name__ == "__main__":
    main()
