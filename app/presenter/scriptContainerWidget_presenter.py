class ScriptContainerWidgetPresenter(object):
    def __init__(self, view):
        self.view = view
        self.view.buttonRunScriptSignal.connect(self.btn_click)

    @staticmethod
    def btn_click(command):
        print(command)
