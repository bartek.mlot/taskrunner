from app.comunicateSignals import CommunicateSignals
from app.widget.scriptContainerWidget import ScriptAreaWidget


class LayoutScriptContainerWidget:
    def __init__(self, layout):
        self.layout = layout
        CommunicateSignals.buttonAddScriptSignal.connect(lambda: self.btn_click())

    def btn_click(self):
        script_container_widget = ScriptAreaWidget()
        self.layout.insertWidget(0, script_container_widget.view)
        
