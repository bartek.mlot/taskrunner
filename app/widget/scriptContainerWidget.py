from app.view.scriptContainer_view import ScriptContainerWidgetView
from app.presenter.scriptContainerWidget_presenter import ScriptContainerWidgetPresenter


class ScriptAreaWidget(object):
    def __init__(self):
        super().__init__()
        self.view = ScriptContainerWidgetView()
        self.presenter = ScriptContainerWidgetPresenter(self.view)
