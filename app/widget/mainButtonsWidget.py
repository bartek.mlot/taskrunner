from app.view.mainButtonsWidget_view import MainButtonsWidgetView
from app.presenter.mainButtonsWidget_presenter import MainButtonsWidgetPresenter


class MainButtonsWidget(object):
    def __init__(self):
        super().__init__()
        self.view = MainButtonsWidgetView()
        self.presenter = MainButtonsWidgetPresenter(self.view)
