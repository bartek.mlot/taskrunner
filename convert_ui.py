import subprocess
import os


def convert_ui_to_py(ui_directory, py_directory):
    """
    Konwertuje wszystkie pliki .ui znajdujące się w ui_directory do plików .py w py_directory,
    używając pyside2-uic.
    """
    for ui_file_name in os.listdir(ui_directory):
        if ui_file_name.endswith('.ui'):
            ui_file_path = os.path.join(ui_directory, ui_file_name)
            py_file_name = os.path.splitext(ui_file_name)[0] + '.py'
            py_file_path = os.path.join(py_directory, py_file_name)

            command = f'pyside2-uic {ui_file_path} -o {py_file_path}'
            subprocess.run(command, shell=True, check=True)
            print(f'Converted {ui_file_name} to {py_file_name}')


ui_directory = './app/ui'
py_directory = './app/ui'
convert_ui_to_py(ui_directory, py_directory)
